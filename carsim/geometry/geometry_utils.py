import math

class GeometryUtils:
    """ Utility functions for geometry """

    @staticmethod
    def degrees_to_radians(degrees):
        """ conversion from degrees to radians """
        return (2.0 * math.pi / 360.0) * degrees

    @staticmethod
    def radians_to_degrees(radians):
        """ Conversion from radians to degrees """
        return radians * (180.0 / math.pi)

    @staticmethod
    def distance(speed, dt):
        """ Speed in m/s and dt in s """
        # TODO: think over positive and negative speeds
        return speed * dt

    @staticmethod
    def end_position(heading, distance):
        """ Heading in radians, distance in meters """
        x = distance * math.cos(heading)
        y = distance * math.sin(heading)
        return x, y, 0.0
