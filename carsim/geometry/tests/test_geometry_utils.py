from unittest import TestCase
from ..geometry_utils import GeometryUtils


class GeometryUtilsTest(TestCase):
    def test_degrees_to_radians(self):
        self.assertEqual(GeometryUtils.degrees_to_radians(0.0), 0.0)
        self.assertEqual(round(GeometryUtils.degrees_to_radians(100.0), 3), 1.745)

    def test_radians_to_degrees(self):
        self.assertEqual(round(GeometryUtils.radians_to_degrees(6.0), 3), 343.775)

    def test_distance(self):
        speed = 20.0 # m/s
        delta_t = 1.0 # s
        self.assertEqual(round(GeometryUtils.distance(speed, delta_t), 3), 20.0)
        
    def test_end_forward_position(self):
        heading = GeometryUtils.degrees_to_radians(90.0)
        distance = 5.0
        end_position = GeometryUtils.end_position(heading, distance)
        
        self.assertEqual(round(end_position[0], 3), 0.0)
        self.assertEqual(round(end_position[1], 3), 5.0)

    def test_end_right_position(self):
        heading = GeometryUtils.degrees_to_radians(0.0)
        distance = 5.0
        end_position = GeometryUtils.end_position(heading, distance)
        
        self.assertEqual(round(end_position[0], 3), 5.0)
        self.assertEqual(round(end_position[1], 3), 0.0)

