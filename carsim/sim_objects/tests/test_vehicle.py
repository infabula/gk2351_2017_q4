from unittest import TestCase
from ..vehicle import Vehicle
from ..vehicle_exceptions import NegativeNumberException

class VehicleTest(TestCase):
    # tests
    def test_set_valid_nb_of_axis(self):
        # arrange
        self.vehicle = Vehicle("Volvo", nb_of_axis=3, weight=3001.0)
        # act
        self.vehicle.nb_of_axis = 4
        # assert
        self.assertEqual(self.vehicle.nb_of_axis, 4)

    def test_set_invalid_nb_of_axis(self):
        with self.assertRaises(NegativeNumberException):
            self.vehicle = Vehicle("Volvo",
                                   nb_of_axis=0,
                                   weight=3001.0)
            self.vehicle.nb_of_axis = 0
        
        
    #def test_vehicle_reader
