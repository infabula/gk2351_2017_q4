import csv


class VehicleReader:
    """ VehicleReader class """
    
    @staticmethod
    def read(filename):
        """ reads a csv file """
        with open(filename, 'r') as fin:
            reader = csv.DictReader(fin)
            for row in reader:
                # create a new vehicle rom the data
                print(row)


if __name__ == "__main__":
    VehicleReader.read("vehicles.csv")            

                
