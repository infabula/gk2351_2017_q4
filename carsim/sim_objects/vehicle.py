from .sim_object import SimObject
from .vehicle_exceptions import NegativeNumberException


class Vehicle(SimObject):
    def __init__(self,
                 manufacturer="",
                 nb_of_axis=2,
                 weight=0.0,
                 position=[0.0, 0.0],
                 heading=0.0,
                 acceleration=0.0):
        super().__init__(weight, position, heading, acceleration)

    @property
    def manufacturer(self):
        return self._manufacturer

    @manufacturer.setter
    def manufacturer(self, m):
        self.manufacturer = m

    @property
    def nb_of_axis(self):
        return self._nb_of_axis

    @nb_of_axis.setter
    def nb_of_axis(self, nb):
        if nb > 0:
            self._nb_of_axis = nb
        else:
            raise NegativeNumberException("Invalid nb_of_axis")
            
            
def test():
    """ Testing the SimObject """
    v = Vehicle("Volvo", nb_of_axis=3, weight=3001.0)
    print("Weight: {}".format(v.weight))
    
# call *run* if this file is not imported
if __name__ == "__main__":
    test()            
