from .vehicle import Vehicle
from .wheel import Wheel

class Truck(Vehicle):
    """ Truck class """
    def __init__(nb_of_axis=3, wheel_radius=0.4):
        """ Contructor """
        self.axis = []
        for axis in nb_of_axis:            
            self.axis.append(Wheel(wheel_radius),
                             Wheel(wheel_radius))
