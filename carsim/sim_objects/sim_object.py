""" Module SimObject"""
class SimObject:
    """ SimObject class"""
    def __init__(self, weight=0.0,
                 position=[0.0, 0.0, 0.0],
                 heading=0.0,
                 speed=0.0,
                 acceleration=0.0):
        """ Constructor"""
        print("Initizalizing SimObject")
        self.weight = weight # uses the setter
        self._position = position
        self._speed = speed
        self.set_heading(heading)
        self._acceleration = acceleration

    def set_heading(self, heading):
        self._heading = heading

    def get_heading(self):
        return self._heading

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, w):
        if w >= 0.0:
            self._weight = w
        else:
            raise Exception("Invalid weight")

    def update(self, delta_t):
        """ update the time dependent properties."""
        current_speed = self._speed # TODO: needs acceleration 
        self._speed = current_speed
        distance = speed * delta_t
        
        
def test():
    """ Testing the SimObject """
    sim_o = SimObject()
    sim_o.weight = 50.5
    print("Weight: {}".format(sim_o.weight))
    
# call *run* if this file is not imported
if __name__ == "__main__":
    test()

