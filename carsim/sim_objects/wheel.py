from .sim_object import SimObject


class Wheel(SimObject):
    """ Wheel class """
    def __init__(radius):
        self.radius = radius
