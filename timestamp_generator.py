from datetime import datetime, timedelta

def timestamp_generator(start_time, millisecond, end_time=None):
    '''
    
    '''
    current_timestamp = start_time
    dt = timedelta(milliseconds=milliseconds)
    while(start_time <= end_time):
        yield current_timestamp
        current_timestamp += dt
        
start_time = datetime.now()
print(start_time)

for timestamp in timestamp_generator(start_time, 1000):
    print(timestamp)
