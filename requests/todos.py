import requests
import json


class TodoRepository:
    def __init__(self):
        self.url = "https://jsonplaceholder.typicode.com/"
        
    def getAll(self):
        response = requests.get(self.url + "todos/")
        if response.status_code == 200:
            response_dict = json.loads(response.text)
            return response_dict

    def getOne(self, id):
        pass

    def getCompleted(self):
        pass

    def create(self, userId, title, completed=False):
        pass

if __name__ == "__main__":
    repository = TodoRepository()
    try:
        for todo in repository.getAll():
            print(todo["title"])
    except Exception as e:
        print(e)
    
