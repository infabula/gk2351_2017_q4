import requests
import json

class Github:
    @staticmethod
    def get_repos(user):
        """ gets the repositories of <user> """

        url = "https://api.github.com/users/" + user + "/repos"

        # do a GET request
        print("Getting data from {}".format(url))
        response = requests.get(url)

        # test for success
        if response.status_code == 200:
            response_dict = json.loads(response.text) # convert to python dict
            for repo in response_dict:
                if "name" in repo:
                    print("- " + repo["name"])
        else:
            print("Could not fetch data from {}".format(url))
  

if __name__ == "__main__":
    Github.get_repos("octocat")            
            
