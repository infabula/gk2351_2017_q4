import requests
import mock

def get_repos():
    print("Get repos")
    r = requests.get("https://api.github.com/users/octocat/repos")
    return r.status_code == 200


@mock.patch('requests.get')
def test_get_repos_passing(mocked_get):
   mocked_req_obj = mock.Mock()
   mocked_req_obj.status_code = 200
   mocked_get.return_value = mocked_req_obj
   assert(get_repos())

   mocked_get.assert_called()
   mocked_get.assert_called_with("https://api.github.com/users/octocat/repos")
