import mock
import csv
import io


def read_from_file(filename):
    """ reads a csv file """
    with open(filename, 'r') as fin:
        reader = csv.DictReader(fin)
        for row in reader:
            # create a new vehicle rom the data
            print(row)

def read_from_stream_io(filename):
    print("read_from_stream_io")
    content = """\
one, two, three
two, two, three
three, two, three"""
    
    fin = io.StringIO(content)
    reader = csv.DictReader(fin)
    for row in reader:
        print(row)
    
'''
@mock.patch('open')
def test_read_from_file(mocked_open):
'''    
    

if __name__ == "__main__":
    read_from_stream_io("data.csv")
    read_from_file("data.csv")
