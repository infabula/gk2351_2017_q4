from unittest import TestCase
import mock
from github import Github


class GithubTest(TestCase):
    """ Test class for Github class"""

    # patch github.requests.get --> will becomes 'mocked_get' (arg1)
    
    @mock.patch('github.requests.get') # patch specifically the imported requests of the github module
    def test_github_get_repos_passing(self, mocked_get):
        # setup
        mocked_response_object = mock.Mock() # create mocked request object
        mocked_response_object.status_code = 200 # simulate a success response
        mocked_response_object.text = '[{"name": "mocked repo"}, {"name": "voila"}]'
        mocked_get.return_value = mocked_response_object # assign new return value

        # act
        Github.get_repos("octocat")

        # assert
        mocked_get.assert_called()
        mocked_get.assert_called_with("https://api.github.com/users/octocat/repos")
