def is_of_int_type(old_function):
    def type_checked(*argv):
        for argument in argv:
            if type(argument) != int:
                raise Exception("Argument is not of integer type")
        return old_function(*argv)
    return type_checked


@is_of_int_type
def multiply(a, b):
    return a * b

print(multiply(4, 5))
print(multiply(4.0, 5))


