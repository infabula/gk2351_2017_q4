import sys


def process_arguments():
    """ Process program arguments."""
    if len(sys.argv) > 1:
        pass # process arguments here

def run():
    """ Starting point of the program."""
    try:
        process_arguments()
        # more code ....

    # catch all uncought exceptions   
    except Exception as e:
        print(e)
        sys.exit(-1)
    sys.exit(0)

# call *run* if this file is not imported
if __name__ == "__main__":
    run()
